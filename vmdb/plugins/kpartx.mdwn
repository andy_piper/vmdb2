Step: kpartx
-----------------------------------------------------------------------------

Create loop devices for partitions in an image file. Not needed when
installing to a real block device, instead of an image file.

Step keys:

* `kpartx` &mdash; REQUIRED; filename of block device with partitions.

* `tags` &mdash; OPTIONAL; list of tags to apply to partitions when re-using an existing
  image that has already been populated with formatted partitions. This can be useful in
  scenarios where an appliance disk image is being built: There needs to be a "debug"
  version of the appliance that provides direct SSH access, the use of `sudo`, etc., and
  also a "production" version that does not. Otherwise the disk images need to be
  identical to aid in diagnosing issues found in production environments. The production
  and debug images can be produced by first creating the "release" version using
  `vmdb2`, and then passing the production disk image to a second `vmdb` where developer
  access is configured.

Example (in the .vmdb file):

    # typical use
    - kpartx: "{{ output }}"

    # using an image that already contains partitions containing filesystems
    # that should be mounted as `/boot` and `/`
    - kpartx: "{{ output }}"
      tags:
        - boot
        - root
